# 3.1 SQL应用汇

## SQL代理作业直接发钉钉机器人消息

> @一寸超人

```sql
ALTER TRIGGER [dbo].[DDtx] 
   ON [dbo].[ES_Witodo] 
   AFTER INSERT
AS 
BEGIN    
    SET NOCOUNT ON;    
	declare @PostData nVARCHAR(max)  ,
	@ResponseText VARCHAR(max) , 
	@sql nVARCHAR(max) 
	set @sql= ……………………  )
set @ResponseText=''
DECLARE @ServiceUrl   VARCHAR(1000) 
set @ServiceUrl = N'https://oapi.dingtalk.com/robot/send?access_token=…………………………'
set @PostData = N'{"msgtype": "markdown", "markdown": { 
"title":"机器人", 
"text": "
### '+@sql+'\n'+' 
来自**派单机器人**的通知
" } }'  
DECLARE @Object AS INT ,
@status INT ,
@returnText AS VARCHAR(8000) ,
@HttpStatus VARCHAR(200) ,
@HttpMethod VARCHAR(20) 
set @HttpMethod= 'post'
EXEC @status = sp_OACreate 'Msxml2.ServerXMLHTTP.3.0', @Object OUT;
EXEC @status = sp_OAMethod @Object, 'open', NULL, @HttpMethod, @ServiceUrl, 'false'
EXEC @status = sys.sp_OAMethod @Object, 'setRequestHeader', NULL, 'Content-Type', 'application/json; charset=UTF-8'
EXEC @status = sp_OAMethod @Object, 'send', NULL, @PostData
EXEC @status = sys.sp_OAGetProperty @Object, 'Status', @HttpStatus OUT;
EXEC @status = sp_OAMethod @Object, 'responseText', @ResponseText OUTPUT
EXEC @status = sp_OADestroy @Object
print @ResponseText
```

## 应付账款账龄计算公式
> @简单生活  
```sql
---查询应收应付账龄
SELECT
	类型,
	简称,
	SUM (应收付金额) '应收付金额',
	---0 AS '账期内本币金额',
	'Days30' = SUM ( CASE WHEN 逾期天数 >= 0 AND 逾期天数 < 30 THEN 应收付金额 ELSE 0 END	 ),
	'Days60' = SUM ( CASE WHEN 逾期天数 >= 30 AND 逾期天数 < 60 THEN 应收付金额 ELSE 0 END ),
	'Days90' = SUM ( CASE WHEN 逾期天数 >= 60 AND 逾期天数 < 90 THEN 应收付金额 ELSE 0 END ),
	'Days180' = SUM ( CASE WHEN 逾期天数 >= 90 AND 逾期天数 < 180 THEN 应收付金额 ELSE 0 END ),
	'Days270' = SUM ( CASE WHEN 逾期天数 >= 180 AND 逾期天数 < 270 THEN 应收付金额 ELSE 0 END ),
	'Days360' = SUM ( CASE WHEN 逾期天数 >= 270 AND 逾期天数 < 360 THEN 应收付金额 ELSE 0 END )
FROM
	vFI_RP
GROUP BY
	类型,
	简称
```

## 统计ES开发
?> 据说高手的平均模板编辑次数不超过10次，你做到了吗？  

![](./3.1.1.jpg)  
> @挨踢熊  
```sql
--统计模板更新
select rtid,
	rtno 编号编号,
	rtname 模板名称,
	classid,
	creuname 创建人,
	cretime 创建时间,
	updtime 更新时间,
	ver 
from es_tmp
order by ver desc
--统计总更新
select sum(ver) 总编辑次数,
	count(rtid) 模板数量,
	max(ver) 最高编辑次数,
	sum(ver)/count(rtid) 平均每个模板编辑次数 
from es_tmp
```

## 查看数据库各表所占空间大小（分析优化必备）
```sql
CREATE TABLE #a (name varchar(265),
        rows bigint,
        reserved varchar(265),
        data varchar(265),
        index_size varchar(265),
        unused varchar(265)
)
EXEC sp_msforeachtable 'INSERT INTO #a exec sp_spaceused''?'''
SELECT * FROM #a order by rows desc
drop table #a
```

## SQL根据身份证判断性别
![](./3.1.1.png)

## 恢复工作台记录丢失
工作台偶尔有记录会丢失，但数据库中却存在  
```sql
insert into es_repcase (rcid,rtId,fillDept,fillDeptName,fillUser,fillUserName,state,
lstFiller,lstFillerName,lstFillDate,wiId)
select excelserverRcid,excelserverRtId,u.deptId,u.deptName,u.userId,u.userName,1,
u.userId,u.userName,getdate(),excelserverWiId
from 销售订单主表 a,es_v_user u
where excelserverrcid not in (select rcid from es_repcase)
and a.录入人=u.userName
```

!> 注意：代码中的`销售订单主表`改成丢失记录模板的主表名称

## 恢复卡住的工作流
!> 工作流很容易在办理时因强退或其他原因卡在**进行中待办**且不易被发现  
可以通过设置定时自动任务或SQL代理扫描恢复  
```sql
--恢复卡住的工作流
update ES_WorkItem
set state=0
where state=1 and not exists (select * from ESSystem..ES_SesLog where UserId=ES_WorkItem.CheckOutBy)
--查询卡住的工作流
select a1.*,a2.ip from ES_WorkItem a1 left outer join ESSystem..ES_SesLog a2 
on a1.CheckOutBy = a2.UserId
where a1.state=1 
--查询卡住的工作流详情(含任务名称)
SELECT A1.wiId , A1.CreByName, A1.State,
      A1.CheckOutByName, A1.ComByName, A3.pName, 
      A4.IP, A1.CreDate, A1.state1,A1.RCID,A5.rcDesc
FROM dbo.ES_WorkItem A1 
INNER JOIN dbo.ES_WfCase A2 ON A1.piId = A2.piId 
INNER JOIN dbo.ES_WorkFlow A3 ON A2.pId = A3.pId 
LEFT OUTER JOIN ESSystem.dbo.ES_SesLog A4 ON A1.CheckOutBy = A4.UserId 
left outer join dbo.ES_RepCase A5 ON A1.rcid = A5.rcid
WHERE (A1.State = 1)
```

## 批量修改模板保存后自动关闭
```sql
UPDATE es_tmpprop
set closeaftersave=1
```

## 查询待办事宜
> @Kang
```sql
 SELECT A1.wiid AS 工作流序号,
       A1.CreByName AS 创建人,
       A1.State AS 工作流状态,
       A1.CheckOutByName AS 进行人,
       A1.ComByName AS 暂存人,
       A1.State1 AS 暂存状态,
       A1.CreDate AS 创建时间,
       A3.PName AS 描述,
       A1.WiDesc AS 说明,
       A4.IP,
       A6.username AS 待办人,Email
FROM dbo.es_workitem A1 
     INNER JOIN dbo.es_wfcase A2 ON A1.piid = A2.piid 
     INNER JOIN dbo.es_workflow A3 ON A2.pid = A3.pid 
     LEFT JOIN ESSystem.dbo.ES_Seslog A4 ON A1.checkoutby = A4.UserId 
     LEFT JOIN es_Witodo AS A5 ON A1.Wiid=A5.wiid 
     LEFT JOIN es_user AS A6 ON A5.userid=A6.userid
WHERE (A1.State<>2)
```

## 解决存储过程调用异常(连接无法用于执行此操作)
> @Meteor  
![](./3.1.2.png)

> @Kang  
```sql
SET NOCOUNT ON 
--原存储过程
SET NOCOUNT OFF
```

## 递归查询科目全称(asked by @昆明haotian) 
![](./3.1.3.png)

![](./3.1.4.png)

```sql
--select id, name from iName --iName是原表，只有科目代码id,和科目名称name
--go
--以下为视图代码
with iName2(id,name,fullname) as 
(
	select ID, name, CAST(name as varchar(4000)) from iName where LEN(id)=2
	union all
	select a.id, a.name, CAST(b.fullname +'-'+ a.name as varchar(4000))
		from iName a, iName2 b where LEFT(a.id, LEN(a.id)-2)=b.id 
)
select id,name,fullname from iName2 --求得全称
```

## 外部数据源中的视图如何使用(asked by @訫訫) 
![](./3.1.5.png)

>@清风 直接写SQL，注意路径：[外部数据源名称].[数据库实体名].[表]

## 查询SQL耗时高的操作（后期优化必备）

```
SELECT TOP 20
    total_worker_time/1000 AS [总消耗CPU 时间(ms)],execution_count [运行次数],
    qs.total_worker_time/qs.execution_count/1000 AS [平均消耗CPU 时间(ms)],
    last_execution_time AS [最后一次执行时间],max_worker_time /1000 AS [最大执行时间(ms)],
    SUBSTRING(qt.text,qs.statement_start_offset/2+1, 
        (CASE WHEN qs.statement_end_offset = -1 
        THEN DATALENGTH(qt.text) 
        ELSE qs.statement_end_offset END -qs.statement_start_offset)/2 + 1) 
    AS [使用CPU的语法], qt.text [完整语法],
    dbname=db_name(qt.dbid),
    object_name(qt.objectid,qt.dbid) ObjectName
FROM sys.dm_exec_query_stats qs WITH(nolock)
CROSS apply sys.dm_exec_sql_text(qs.sql_handle) AS qt
WHERE execution_count>1
ORDER BY  total_worker_time DESC
```

## SQL整数二进制移位(左移右移)

> 栗子：16进制，高8位低8位互换：
```sql 
select (@SQLVal & 255 )*256+(@SQLVal & 65280 )/256 --高低字节互换
```

## NX同步其他账套组织架构
```sql
--清空架构
truncate table JU_Org
truncate table JU_Role
truncate table JU_Post
truncate table JU_PostEmployee
truncate table JU_User
truncate table JU_UserOrg
truncate table JU_UserPost
truncate table JU_UserEmployee
truncate table JU_DingDUser
truncate table JU_QYWXUser
--插入来源架构
insert into JU_Org select * from 来源数据库.dbo.JU_Org
insert into JU_Role select * from 来源数据库.dbo.JU_Role
insert into JU_Post select * from 来源数据库.dbo.JU_Post
insert into JU_PostEmployee select * from 来源数据库.dbo.JU_PostEmployee
insert into JU_User select * from 来源数据库.dbo.JU_User
insert into JU_UserOrg select * from 来源数据库.dbo.JU_UserOrg
insert into JU_UserPost select * from 来源数据库.dbo.JU_UserPost
insert into JU_UserEmployee select * from 来源数据库.dbo.JU_UserEmployee
insert into JU_DingDUser select * from 来源数据库.dbo.JU_DingDUser
insert into JU_QYWXUser select * from 来源数据库.dbo.JU_QYWXUser
--重置ID
update JU_Seed set Seed = src.Seed from 来源数据库.dbo.JU_Seed src where JU_Seed.Keyword = src.Keyword and JU_Seed.Keyword in(
'JU_Role.RoleID',
'JU_Post.PostID',
'JU_User.UserID',
'JU_UserOrg.UserOrgID',
'JU_UserPost.UserPostID')
```

## 实用工具函数
```sql
/* 获取文字规格中最后段数字，例如内盒100*80*20的20 */
create FUNCTION [dbo].[GET_LastNUM](@S VARCHAR(100))
RETURNS bigINT
AS
BEGIN
--取出最后一段
set @S=reverse(stuff(reverse(@S),charindex('*',reverse(@S)),100,''))
--去除非数字
WHILE PATINDEX('%[^0-9]%',@S) > 0
BEGIN
set @s=stuff(@s,patindex('%[^0-9]%',@s),1,'')
END
RETURN cast(@S as bigINT)
END
/* 获取文字规格中第一个段数字，例如内盒100*80*20的100 */
create FUNCTION [dbo].[GET_FirstNUM](@S VARCHAR(100))
RETURNS bigINT
AS
BEGIN
set @S=stuff(@S,charindex('*',@S),100,'')
WHILE PATINDEX('%[^0-9]%',@S) > 0
BEGIN
set @s=stuff(@s,patindex('%[^0-9]%',@s),1,'')
END
RETURN cast(@S as bigINT)
END
/* 解包数字，例如1-5 返回1,2,3,4,5 */
create FUNCTION [dbo].[unpackNum](@String varchar(8000))
returns varchar(8000)
as
begin
    declare @idx int
	declare @last int
	declare @current int
	declare @ret varchar(8000)
    declare @slice varchar(8000)
	set @ret=''
    select @idx = 1
          if len(@String)<1 or @String is null return @ret
    while @idx!= 0
    begin
        set @idx =charindex(',',@String)
        if @idx!=0
            set @slice =left(@String,@idx - 1)
        else
            set @slice = @String
        if(len(@slice)>0)
			begin
				if charindex('-',@slice)>0 
				begin
					select @current=left(@slice,charindex('-',@slice)-1)
						,@last=right(@slice,len(@slice)-charindex('-',@slice))
					while @current<=@last						
					begin
						set @ret=concat(@ret,',',cast(@current as varchar))
						set @current=@current+1
					end
				end
				else 
					set @ret=concat(@ret,',',@slice)				
			end
        set @String =right(@String,len(@String)- @idx)
        if len(@String)= 0 break
    end
return stuff(@ret,1,1,'')                                                                                                                                                                                                                    
end
/* 打包数字，例如1,2,3,4,5返回1-5 主要用于装箱打托盘清单打印等 */
create FUNCTION [dbo].[packNum](@String varchar(8000))
returns varchar(8000)
as
begin    
	declare @ret varchar(8000);
	with t as (
		select top 1000 cast(a as int) a from dbo.split(@String,',') order by a
	), tt as (
		select *
		,b=(select 1 from t where a=raw.a+1)
		,c=(select 1 from t where a=raw.a-1)
		 from t raw
	 ), ttt as (
		select concat(a,iif(b=1,'-',',')) a
		from tt where b is null or c is null
	)
	select @ret=dbo.strjoin(a,'')
	from ttt
return @ret                                                                                                                                                                                                                    
end
/* 检查包含关系,逗号分隔，顺序可乱 */
create function [dbo].[fWithin](@in varchar(100),@str varchar(100))
returns  int
as
begin
declare @word varchar(100)
if CHARINDEX('kcmy', @in)>0
begin
return 0
end
set @in=replace(rtrim(@in),'，',',')+','
set @str=','+replace(rtrim(@str),'，',',')+','
while CHARINDEX(',',@in)>0
begin
set @word=LEFT(@in,charindex(',',@in)-1)
if CHARINDEX(','+@word+',',@str)=0
begin
return 0
end
set @in=STUFF(@in,1,charindex(',',@in),'')
end
return 1
end
/* split精准检索版，@N为要检索的行号，负数视为反检索 
 * 例子：
 * select dbo.splitN('1000*2000*3000*4000*5000', '*', 1) --返回1000
 * select dbo.splitN('1000*2000*3000*4000*5000', '*', 2) --返回2000
 * select dbo.splitN('1000*2000*3000*4000*5000', '*', -1) --返回5000
 * select dbo.splitN('1000*2000*3000*4000*5000', '*', -2) --返回4000  */
create function [dbo].[splitN](
@SourceSql varchar(8000),
@StrSeprate varchar(10),
@N int
)
returns varchar(8000) 
begin 
    declare @i int
	declare @rn as int=1
	declare @str varchar(8000) 
	declare @temp table(a varchar(100),i int)
    set @SourceSql=rtrim(ltrim(@SourceSql))
    set @i=charindex(@StrSeprate,@SourceSql)
    while @i>=1 
    begin
        insert @temp values(left(@SourceSql,@i-1),@rn)
		set @rn=@rn+1
        set @SourceSql=substring(@SourceSql,@i+1,len(@SourceSql)-@i)
        set @i=charindex(@StrSeprate,@SourceSql)
    end
    if @SourceSql<>'\'
       insert @temp values(@SourceSql,@rn)
	if @N<0
	   set @N=(select max(i)+1+@N from @temp )
	set @str=(select a from @temp where i=@N)
    return @str
end
--对给定分割符的字符串去重，例如'123,abc,234,bc,123,bc'返回'123,abc,234,bc'
alter function [dbo].[replaceN](
@SourceSql varchar(8000),
@StrSeprate varchar(10)
--@N int
)
returns varchar(8000) 
begin 
    declare @i int
	declare @rn as int=1
	declare @str varchar(8000) 
	declare @temp table(a varchar(100),i int)
    set @SourceSql=rtrim(ltrim(@SourceSql))
    set @i=charindex(@StrSeprate,@SourceSql)
    while @i>=1 
    begin
        insert @temp select left(@SourceSql,@i-1),@rn where left(@SourceSql,@i-1) not in (select a from @temp)
		set @rn=@rn+1
        set @SourceSql=substring(@SourceSql,@i+1,len(@SourceSql)-@i)
        set @i=charindex(@StrSeprate,@SourceSql)
    end
	set @str=(select dbo.strjoin(a,@StrSeprate) from @temp )
    return @str
end
```

## ESAP远程执行bat批处理文件

> @一影成城:不同服务器 我用SQLserver 的这条命令`exec master..xp_cmdshell "C:\FRP\stop.bat"`

要写条存储过程，再用esap查询功能运行就可以了
要先启用 cmdshell 
 
```sql
EXEC sp_configure 'show advanced options', 1;
RECONFIGURE;
EXEC sp_configure 'xp_cmdshell', 1;
RECONFIGURE;
```

要关只是将上面的后面的那个”1”改成”0”就可以了。
不过stop.bat 里要加多句代码 
cd c:\frp 才能运行成功

## 获取cpu ProcessorId

> @平淡人生: 
 
```sql
--获取cpu ProcessorId
if object_id('tempdb..#tb') is not null drop table #tb
create table #tb(id [int] IDENTITY(1,1),re varchar(255))
 exec sp_configure 'show advanced options', 1;reconfigure;exec sp_configure 'xp_cmdshell', 1;
 reconfigure
 insert into #tb exec master..xp_cmdshell 'wmic cpu get ProcessorId'
select ltrim(rtrim(re)) as ProcessorId from #tb where id =2--注意空格
```

